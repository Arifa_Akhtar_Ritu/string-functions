<?php

$array = ['lastname', 'email', 'phone'];
var_dump(implode(",", $array)); // string(20) "lastname,email,phone"

// Empty string when using an empty array:
var_dump(implode('ritu', [])); // string(0) ""

// The separator is optional:
var_dump(implode(['r', 'i', 't'])); // string(3) "abc"

?>